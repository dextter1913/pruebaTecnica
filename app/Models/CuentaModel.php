<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CuentaModel extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fields = [
        'idCuenta',
        'nombre',
        'email',
        'telefono'
    ];

    protected $table = 'cuenta';
}

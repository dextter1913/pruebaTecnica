<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductosModel extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fields = [
        'idProducto',
        'nombre',
        'descripcion',
        'valor'
    ];

    protected $table = 'productos';
}

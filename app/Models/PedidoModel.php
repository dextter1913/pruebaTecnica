<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PedidoModel extends Model
{
    use HasFactory;
    use SoftDeletes;


    protected $fields = [
        'idPedido',
        'idCuenta',
        'idProducto',
        'cantidad',
        'total',
        'created_at',
        'updated_at'
    ];

    protected $table = 'pedido';
}

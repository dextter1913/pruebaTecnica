<?php

namespace App\tools;

use Illuminate\Support\Facades\Validator;

/**
 * cambio para probar hook
 */
class validaciones
{

    /**
     * Tool para hacer una validacion de datos para crear cuenta
     * 
     * @param object
     * @return object
     */
    public static function validarCreateCuenta(object  $data): array
    {
        $data = $data->validate([
            'nombre'   => 'required|string|max:50',
            'email'    => 'required|string|max:50',
            'telefono' => 'required|string|max:50'
        ]);
        return $data;
    }


    /**
     * Tool para hacer una validacion de datos para actualizar cuenta
     * 
     * @param object
     * @return object
     */
    public static function validarUpdateCuenta(object  $data): array
    {
        $data = $data->validate([
            'idCuenta' => 'required|integer',
            'nombre'   => 'required|string|max:50',
            'email'    => 'required|string|max:50',
            'telefono' => 'required|string|max:50'
        ]);
        return $data;
    }


    /**
     * Tool para validar mostrar los datos para mostrar cuenta
     * 
     * @param object
     * @return object
     */
    public static function validarGetCuentas(object $data): array
    {
        $data = $data->validate([
            'idCuenta' => 'required|integer'
        ]);
        return $data;
    }


    /**
     * Tool para validar eliminacion logica de datos de cuenta
     * 
     * @param object
     * @return object
     */
    public static function validarDeleteCuentas(object $data): array
    {
        $data = $data->validate([
            'idCuenta' => 'required|integer'
        ]);
        return $data;
    }


    /**
     * Tool para validar datos de entrada para crear productos
     * 
     * @param object
     * @return object
     */
    public static function validarCreateProductos(object | array $data): array
    {
        $data = Validator::make($data, [
            'nombre'      => 'required|string|max:60',
            'descripcion' => 'required|string|max:255',
            'valor'       => 'required|integer'
        ]);
        return $data->validate();
    }


    /**
     * Tool para validar datos de entrada para actualizar productos
     * 
     * @param object
     * @return object
     */
    public static function validarUpdateProductos(object $data): array
    {
        $data = $data->validate([
            'idProducto' => 'required|integer',
            'nombre'     => 'required|string|max:60',
            'descripcion' => 'required|string|max:255',
            'valor'      => 'required|integer'
        ]);
        return $data;
    }


    /**
     * Tool para validar datos de entrada para mostrar productos
     * 
     * @param object
     * @return object
     */
    public static function validarGetProductos(object $data): array
    {
        $data = $data->validate([
            'idProducto' => 'required|integer'
        ]);
        return $data;
    }


    /**
     * Tool para validar datos de entrada para eliminar productos
     * 
     * @param object
     * @return object
     */
    public static function validarDeleteProductos(object $data): array
    {
        $data = $data->validate([
            'idProducto' => 'required|integer'
        ]);
        return $data;
    }


    /**
     * Tool para validar datos de entrada para crear pedidos
     * 
     * @param object
     * @return object
     */
    public static function validarCreatePedidos(object | array $data): array
    {
        $data = Validator::make($data, [
            'idCuenta'   => 'required|integer',
            'idProducto' => 'required|integer',
            'cantidad'   => 'required|integer'
        ]);
        return $data->validate();
    }


    /**
     * Tool para validar datos de entrada para mostrar pedidos
     * 
     * @param object
     * @return object
     */
    public static function validarGetPedidos(object $data): array
    {
        $data = $data->validate([
            'idPedido' => 'required|integer'
        ]);
        return $data;
    }


    /**
     * Tool para validar datos de entrada para actualizar pedidos
     * 
     * @param object
     * @return object
     */
    public static function validarUpdatePedidos(object $data): array
    {
        $data = $data->validate([
            'idPedido'   => 'required|integer',
            'idCuenta'   => 'required|integer',
            'idProducto' => 'required|integer',
            'cantidad'   => 'required|integer'
        ]);
        return $data;
    }


    /**
     * Tool para validar datos de entrada para eliminar pedidos
     * 
     * @param object
     * @return object
     */
    public static function validarDeletePedidos(object $data): array
    {
        $data = $data->validate([
            'idPedido' => 'required|integer'
        ]);
        return $data;
    }
}

<?php

namespace App\Http\Controllers;

use App\Services\ProductosService;
use App\tools\validaciones;
use Illuminate\Http\Request;

class ProductosController extends Controller
{
    /**
     * atributo para inyectar tool
     * 
     * @var object
     */
    private $validaciones;

    /**
     * atributo para inyectar service ProductosService
     * 
     * @var object
     */
    private $ProductosService;


    public function __construct(validaciones $validaciones, ProductosService $ProductosService)
    {
        $this->validaciones     = $validaciones;
        $this->ProductosService = $ProductosService;
    }


    /**
     * controlador para insertar datos en productos
     * 
     * @param Illuminate\Http\Request
     * @return Illuminate\Http\JsonResponse
     */
    public function Create(Request $request)
    {
        foreach ($request->all() as $value) {
            $data[] = $this->validaciones->validarCreateProductos($value);
        }

        return response()->json(
            [
                'message' => $this->ProductosService->Create($data)
            ],
            200
        );
    }


    /**
     * controlador para actualizar datos en productos
     * 
     * @param Illuminate\Http\Request
     * @return Illuminate\Http\JsonResponse
     */
    public function Update(Request $request)
    {
        $data = $this->validaciones->validarUpdateProductos($request);
        return response()->json(
            [
                'message' => $this->ProductosService->Update($data)
            ],
            200
        );
    }


    /**
     * controlador para mostrar datos en productos
     * 
     * @param Illuminate\Http\Request
     * @return Illuminate\Http\JsonResponse
     */
    public function Get(Request $request)
    {
        $data = $request->all();
        if (isset($data['idProducto'])) {
            $data = $this->validaciones->validarGetProductos($request);
            return response()->json(
                $this->ProductosService->getProductos($data),
                200
            );
        } else {
            return response()->json(
                $this->ProductosService->getProductos(),
                200
            );
        }
    }


    /**
     * controlador para eliminar datos en productos
     * 
     * @param Illuminate\Http\Request
     * @return Illuminate\Http\JsonResponse
     */
    public function Delete(Request $request)
    {
        $data = $this->validaciones->validarDeleteProductos($request);
        return response()->json(
            [
                'message' => $this->ProductosService->Delete($data)
            ],
            200
        );
    }
}

<?php

namespace App\Http\Controllers;

use App\Services\PedidoService;
use App\tools\validaciones;
use Illuminate\Http\Request;

class PedidoController extends Controller
{
    /**
     * atributo para inyectar tool
     * 
     * @var object
     */
    private $validaciones;


    /**
     * atributo para inyectar service PedidoService
     * 
     * @var object
     */
    private $PedidoService;


    public function __construct(validaciones $validaciones, PedidoService $PedidoService)
    {
        $this->validaciones  = $validaciones;
        $this->PedidoService = $PedidoService;
    }


    /**
     * controlador para insertar datos en cuenta
     * 
     * @param Illuminate\Http\Request
     * @return Illuminate\Http\JsonResponse
     */
    public function Create(Request $request)
    {
        foreach ($request->all() as $key => $value) {
            $data[] = $this->validaciones->validarCreatePedidos($value);
        }

        return response()->json(
            $this->PedidoService->Create($data),
            201
        );
    }


    /**
     * controlador para obtener los datos de la tabla pedidos
     * 
     * @param Illuminate\Http\Request
     * @return Illuminate\Http\JsonResponse
     */
    public function Get(Request $request)
    {
        $data = $request->all();
        if (isset($data['idPedido'])) {
            $data = $this->validaciones->validarGetPedidos($request);
            return response()->json(
                $this->PedidoService->getPedidos($data),
                200
            );
        } else {
            return response()->json(
                $this->PedidoService->getPedidos(),
                200
            );
        }
    }


    /**
     * controlador para actualizar los datos de la tabla pedidos
     * 
     * @param Illuminate\Http\Request
     * @return Illuminate\Http\JsonResponse
     */
    public function Update(Request $request)
    {
        $data = $this->validaciones->validarUpdatePedidos($request);
        return response()->json(
            [
                'message' =>  $this->PedidoService->Update($data)
            ],
            200
        );
    }


    /**
     * controlador para eliminar los datos de la tabla pedidos
     * 
     * @param Illuminate\Http\Request
     * @return Illuminate\Http\JsonResponse
     */
    public function Delete(Request $request)
    {
        $data = $this->validaciones->validarDeletePedidos($request);
        return response()->json(
            [
                'message' =>  $this->PedidoService->Delete($data)
            ],
            200
        );
    }
}

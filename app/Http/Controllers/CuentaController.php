<?php

namespace App\Http\Controllers;

use App\Services\CuentaService;
use App\tools\validaciones;
use Illuminate\Http\Request;


class CuentaController extends Controller
{
    /**
     * atributo para inyectar tool
     * 
     * @var object
     */
    private $validaciones;


    /**
     * atributo para inyectar service CuentaService
     * 
     * @var object
     */
    private $CuentaService;


    public function __construct(validaciones $validaciones, CuentaService $CuentaService)
    {
        $this->validaciones  = $validaciones;
        $this->CuentaService = $CuentaService;
    }


    /**
     * controlador para insertar datos en cuenta
     * 
     * @param Illuminate\Http\Request
     * @return Illuminate\Http\JsonResponse
     */
    public function Create(Request $request)
    {
        $data = $this->validaciones->validarCreateCuenta($request);
        return response()->json(
            [
                'message' => $this->CuentaService->Create($data)
            ],
            200
        );
    }


    /**
     * controlador para modificar datos en cuenta
     * 
     * @param Illuminate\Http\Request
     * @return Illuminate\Http\JsonResponse
     */
    public function Update(Request $request)
    {
        $data = $this->validaciones->validarUpdateCuenta($request);
        return response()->json(
            [
                'message' => $this->CuentaService->Update($data)
            ],
            200
        );
    }


    /**
     * controlador para obtener cuentas registradas
     * 
     * @param Illuminate\Http\Request
     * @return Illuminate\Http\JsonResponse
     */
    public function Get(Request $request)
    {
        $data = $request->all();
        if (isset($data['idCuenta'])) {
            $data = $this->validaciones->validarGetCuentas($request);
            return response()->json(
                $this->CuentaService->getCuentas($data),
                200
            );
        } else {
            return response()->json(
                $this->CuentaService->getCuentas(),
                200
            );
        }
    }


    /**
     * controlador para eliminar cuenta
     * 
     * @param Illuminate\Http\Request
     * @return Illuminate\Http\JsonResponse
     */
    public function Delete(Request $request)
    {
        $data = $this->validaciones->validarDeleteCuentas($request);
        return response()->json(
            [
                'message' => $this->CuentaService->Delete($data)
            ],
            200
        );
    }
}

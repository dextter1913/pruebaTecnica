<?php

namespace App\Services;

use App\Models\CuentaModel;
use App\Models\PedidoModel;
use App\Models\ProductosModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;

class PedidoService
{
    /**
     * caso de uso para insertar Datos en cuenta
     * 
     * @param array
     * @return string
     */
    public function Create(array $data): array | object
    {
        /**
         * variable que almacenara la informacion de la tabla productos
         * 
         * @var array
         */
        (array)$productos = [];


        /**
         * variable que almacenara la informacion de el total del producto
         * 
         * @var integer
         */
        (int)$total = 0;


        /**
         * variable para estructurar el mapa a inyectar en el modelo de pedidos
         * 
         * @var array
         */
        (array)$model = [];


        /**
         * variable para mostrar toda la informacion del pedido, cliente y producto
         * 
         * @var array
         */
        (array)$info = [];


        /**
         * variable para almacenar los productos que existen
         * 
         * @var array
         */
        (array)$exists = [];


        foreach ($data as $value) {
            $productos = ProductosModel::select('valor', 'nombre')
                ->where('idProducto', $value['idProducto'])
                ->get();

            if (count($productos) > 0) {
                $total  = $productos[0]['valor'] * $value['cantidad'];
                $exists = $productos[0];

                $model = [
                    'idCuenta'   => $value['idCuenta'],
                    'idProducto' => $value['idProducto'],
                    'cantidad'   => $value['cantidad'],
                    'total'      => $total
                ];

                PedidoModel::insert($model);
            } else {
                $exists = [
                    'valor'  => 'no existe',
                    'nombre' => 'no existe'
                ];
            }

            $row = CuentaModel::select('nombre', 'email', 'telefono')
                ->where('idCuenta', $value['idCuenta'])
                ->get()[0];

            $info[] = [
                'nombre'       => $row['nombre'],
                'email'        => $row['email'],
                'telefono'     => $row['telefono'],
                'producto'     => $exists['nombre'],
                'cantidad'     => $value['cantidad'],
                'valor unidad' => $exists['valor'],
                'total'        => $total
            ];
        }
        $response = Http::post('https://prueba-tecnicanodejs.herokuapp.com/api/pedidos/', $info);
        return $response->json();
    }


    /**
     * caso de uso para obtener los datos de la tabla pedidos
     * 
     * @return array
     */
    public function getPedidos(array $data = []): array | object
    {
        /**
         * variable que almacenara la informacion de la tabla pedidos
         * 
         * @var array
         */
        (array)$pedidos = [];

        if (isset($data['idPedido'])) {
            $rows = PedidoModel::select('idCuenta', 'idProducto', 'cantidad', 'total')
                ->where('idPedido', $data['idPedido'])
                ->get();
            foreach ($rows as $value) {
                $productos = ProductosModel::select('valor', 'nombre')
                    ->where('idProducto', $value['idProducto'])
                    ->get()[0];

                $cuentas = CuentaModel::select('nombre', 'email', 'telefono')
                    ->where('idCuenta', $value['idCuenta'])
                    ->get()[0];

                $pedidos[] = [
                    'nombre'       => $cuentas['nombre'],
                    'email'        => $cuentas['email'],
                    'telefono'     => $cuentas['telefono'],
                    'producto'     => $productos['nombre'],
                    'cantidad'     => $value['cantidad'],
                    'valor unidad' => $productos['valor'],
                    'total'        => $value['total']
                ];
            }
        } else {
            $rows = PedidoModel::select('idCuenta', 'idProducto', 'cantidad', 'total')
                ->get();
            foreach ($rows as $value) {
                $productos = ProductosModel::select('valor', 'nombre')
                    ->where('idProducto', $value['idProducto'])
                    ->get()[0];

                $cuentas = CuentaModel::select('nombre', 'email', 'telefono')
                    ->where('idCuenta', $value['idCuenta'])
                    ->get()[0];

                $pedidos[] = [
                    'nombre'       => $cuentas['nombre'],
                    'email'        => $cuentas['email'],
                    'telefono'     => $cuentas['telefono'],
                    'producto'     => $productos['nombre'],
                    'cantidad'     => $value['cantidad'],
                    'valor unidad' => $productos['valor'],
                    'total'        => $value['total']
                ];
            }
        }
        return $pedidos;
    }


    /**
     * caso de uso para actualizar los datos de la tabla pedidos
     * 
     * @param array
     * @return string
     */
    public function Update(array $data): string
    {
        /**
         * variable que almacenara la informacion de la tabla productos
         * 
         * @var array
         */
        (array)$productos = [];

        /**
         * variable que almacenara la informacion de el total del producto
         * 
         * @var integer
         */
        (int)$total = 0;


        /**
         * variable para estructurar el mapa a inyectar en el modelo de pedidos
         * 
         * @var array
         */
        (array)$model = [];

        $productos = ProductosModel::select('valor', 'nombre')
            ->where('idProducto', $data['idProducto'])
            ->get();

        if (count($productos) > 0) {
            $total  = $productos[0]['valor'] * $data['cantidad'];
            $exists = $productos[0];

            $model = [
                'idCuenta'   => $data['idCuenta'],
                'idProducto' => $data['idProducto'],
                'cantidad'   => $data['cantidad'],
                'total'      => $total
            ];

            PedidoModel::where('idPedido', $data['idPedido'])
                ->update($model);
        } else {
            $exists = [
                'valor'  => 'no existe',
                'nombre' => 'no existe'
            ];
        }

        $row = CuentaModel::select('nombre', 'email', 'telefono')
            ->where('idCuenta', $data['idCuenta'])
            ->get()[0];

        $info = [
            'nombre'       => $row['nombre'],
            'email'        => $row['email'],
            'telefono'     => $row['telefono'],
            'producto'     => $exists['nombre'],
            'cantidad'     => $data['cantidad'],
            'valor unidad' => $exists['valor'],
            'total'        => $total
        ];

        // Http::post('http://localhost:8000/api/send', $info);
        return 'actualizado';
    }


    /**
     * caso de uso para eliminar los datos de la tabla pedidos
     * 
     * @param array
     * @return string
     */
    public function Delete(array $data): string
    {
        PedidoModel::where('idPedido', $data['idPedido'])
            ->delete();
        return 'eliminado';
    }

}

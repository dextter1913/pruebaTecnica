<?php

namespace App\Services;

use App\Models\CuentaModel;
use Carbon\Carbon;

class CuentaService
{

    /**
     * caso de uso para insertar Datos en cuenta
     * 
     * @param array
     * @return string
     */
    public function Create(array $data)
    {
        CuentaModel::insert($data);
        return 'se insertaron los datos correctamente';
    }


    /**
     * caso de uso para modificar Datos en cuenta
     * 
     * @param array
     * @return string
     */
    public function Update(array $data)
    {
        $row = CuentaModel::where('idCuenta', $data['idCuenta'])->get();
        if (count($row) == 0) {
            return 'error, posiblemente la informacion del id no existe';
        }
        CuentaModel::where('idCuenta', $data['idCuenta'])->update($data);
        return 'se modificaron los datos correctamente';
    }


    /**
     * caso de uso para mostrar Datos en cuenta
     * 
     * @param array
     * @return string
     */
    public function getCuentas(array $data = [])
    {
        if (isset($data['idCuenta'])) {
            $row = CuentaModel::where('idCuenta', $data['idCuenta'])->get();
            if (count($row) == 0) {
                return ['message' => 'error, la informacion del id no existe'];
            }
            $cuenta = CuentaModel::where('idCuenta', $data['idCuenta'])->get();
            $cuenta[0]['created_at'] = Carbon::parse($cuenta[0]['created_at'])->format('Y-m-d H:i:s');
            $cuenta[0]['updated_at'] = Carbon::parse($cuenta[0]['created_at'])->format('Y-m-d H:i:s');
        } else {
            $cuenta = CuentaModel::all();
        }
        return $cuenta;
    }


    /**
     * caso de uso para eliminar Datos en cuenta
     * 
     * @param array
     * @return string
     */
    public function Delete(array $data)
    {
        $row = CuentaModel::where('idCuenta', $data['idCuenta'])->get();
        if (count($row) == 0) {
            return 'error, posiblemente la informacion del id no existe';
        }
        CuentaModel::where('idCuenta', $data['idCuenta'])->delete();
        return 'se elimino el registro correctamente';
    }
}

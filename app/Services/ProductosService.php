<?php

namespace App\Services;

use App\Models\ProductosModel;
use Carbon\Carbon;

class ProductosService
{
    /**
     * caso de uso para insertar Datos en productos
     * 
     * @param array
     * @return string
     */
    public function Create(array $data)
    {
        foreach ($data as $key => $value) {
            ProductosModel::insert($value);
        }
        return 'se insertaron los datos correctamente';
    }


    /**
     * caso de uso para actualizar Datos en productos
     * 
     * @param array
     * @return string
     */
    public function Update(array $data)
    {
        $row = ProductosModel::where('idProducto', $data['idProducto'])->get();
        if (count($row) == 0) {
            return 'error, posiblemente la informacion del id no existe';
        }
        ProductosModel::where('idProducto', $data['idProducto'])->update($data);
        return 'se actualizaron los datos correctamente';
    }


    /**
     * caso de uso para mostrar Datos en cuenta
     * 
     * @param array
     * @return string
     */
    public function getProductos(array $data = [])
    {
        if (isset($data['idProducto'])) {
            $row = ProductosModel::where('idProducto', $data['idProducto'])->get();
            if (count($row) == 0) {
                return ['message' => 'error, la informacion del id no existe'];
            }
            $producto = ProductosModel::where('idProducto', $data['idProducto'])->get();
            $producto[0]['created_at'] = Carbon::parse($producto[0]['created_at'])->format('Y-m-d H:i:s');
            $producto[0]['updated_at'] = Carbon::parse($producto[0]['created_at'])->format('Y-m-d H:i:s');
        } else {
            $producto = ProductosModel::all();
        }
        return $producto;
    }


    /**
     * caso de uso para eliminar Datos en productos
     * 
     * @param array
     * @return string
     */
    public function Delete(array $data)
    {
        $row = ProductosModel::where('idProducto', $data['idProducto'])->get();
        if (count($row) == 0) {
            return ['message' => 'error, la informacion del id no existe'];
        }
        ProductosModel::where('idProducto', $data['idProducto'])->delete();
        return 'se elimino el registro correctamente';
    }
}

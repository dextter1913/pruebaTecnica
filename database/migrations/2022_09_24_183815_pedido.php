<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedido', function (Blueprint $table) {
            $table->id('idPedido');
            $table->unsignedBigInteger('idCuenta');
            $table->unsignedBigInteger('idProducto');
            $table->integer('cantidad');
            $table->integer('total');
            $table->foreign('idCuenta')
                ->references('idCuenta')
                ->on('cuenta');
            $table->foreign('idProducto')
                ->references('idProducto')
                ->on('productos');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedido');
    }
};

<?php

use App\Http\Controllers\CuentaController;
use App\Http\Controllers\PedidoController;
use App\Http\Controllers\ProductosController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => '/v1'], function () {
    Route::group(['prefix' => '/crud'], function () {



        /**
         * crud de cuentas
         * 
         * @param Illuminate\Http\JsonResponse
         */
        Route::post('/crearCuenta', [CuentaController::class, 'Create']);
        Route::put('/modificarCuenta', [CuentaController::class, 'Update']);
        Route::get('/getCuentas', [CuentaController::class, 'Get']);
        Route::delete('/eliminarCuenta', [CuentaController::class, 'Delete']);



        /**
         * crud de productos
         * 
         * @param Illuminate\Http\JsonResponse
         */
        Route::post('/crearProductos', [ProductosController::class, 'Create']);
        Route::put('/modificarProductos', [ProductosController::class, 'Update']);
        Route::get('/getProductos', [ProductosController::class, 'Get']);
        Route::delete('/eliminarProductos', [ProductosController::class, 'Delete']);



        /**
         * crud de pedidos
         * 
         * @param Illuminate\Http\JsonResponse
         */
        Route::post('/crearPedido', [PedidoController::class, 'Create']);
        Route::put('/modificarPedido', [PedidoController::class, 'Update']);
        Route::get('/getPedido', [PedidoController::class, 'Get']);
        Route::delete('/eliminarPedido', [PedidoController::class, 'Delete']);
    });
});
